import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as opt
import numpy as np
import scipy.stats
import pywt
import torch.cuda.amp as amp


def prepare_wavelet(name):
    wavelet = pywt.Wavelet(name)
    filter_length = len(wavelet.filter_bank[0])
    wavelet_kernel = torch.tensor(
        [wavelet.filter_bank[0], wavelet.filter_bank[1]]).reshape(2, filter_length)
    twoD_kernel = torch.einsum('ab,cd->acbd', wavelet_kernel,
                               wavelet_kernel).reshape(4, 1, filter_length, filter_length)
    return twoD_kernel


def apply_wavelet(img, wavelet):
    length = wavelet.shape[3]
    padding = length//2 - 1
    padded = F.pad(img, (padding, padding, padding, padding), mode='circular')
    return F.conv2d(padded, wavelet, stride=2)


def inverse_wavelet(img, wavelet):
    length = wavelet.shape[3]
    padding = length//2 - 1
    unfiltered = F.conv_transpose2d(img, wavelet, stride=2)
    if padding == 0:
        return unfiltered
    # return unfiltered
    y_len = img.shape[2]
    y_center = unfiltered[:, :, padding:-padding, :]
    up_shave = unfiltered[:, :, :padding, :].clone()
    down_shave = unfiltered[:, :, -padding:, :].clone()
    y_center[:, :, -padding:, :] += up_shave
    y_center[:, :, :padding, :] += down_shave

    x_len = y_center.shape[3]
    x_center = y_center[:, :, :, padding:-padding]
    left_shave = y_center[:, :, :, :padding].clone()
    right_shave = y_center[:, :, :, -padding:].clone()
    x_center[:, :, :, -padding:] += left_shave
    x_center[:, :, :, :padding] += right_shave
    return x_center


def apply_wavelet_c(img, wavelet):
    [b, c, h, w] = img.shape
    reshaped = img.reshape(b*c, 1, h, w)
    return apply_wavelet(reshaped, wavelet).reshape(b, c, 4, h//2, w//2)


def inverse_wavelet_c(img, wavelet):
    [b, c, a, h, w] = img.shape
    reshaped = img.reshape(b*c, a, h, w)
    return inverse_wavelet(reshaped, wavelet).reshape(b, c, h*2, w*2)


class EvoNormS02D(nn.Module):
    def __init__(self, in_chans, groups):
        super().__init__()
        assert in_chans % groups == 0
        self.in_chans = in_chans
        self.groups = groups
        self.scale = nn.Parameter(torch.ones(1, in_chans, 1, 1))
        self.shift = nn.Parameter(torch.zeros(1, in_chans, 1, 1))
        self.v1 = nn.Parameter(torch.ones(1, in_chans, 1, 1))

    def forward(self, x):
        [b, c, h, w] = x.shape
        reshaped = x.reshape(b, self.groups, self.in_chans//self.groups, h, w)
        stds = torch.rsqrt(torch.var(reshaped, (2, 3, 4), keepdim=True)+1e-6).expand(
            b, self.groups, self.in_chans//self.groups, 1, 1).reshape(b, c, 1, 1)
        return x*torch.sigmoid(self.v1*x)*stds*self.scale + self.shift

    def __repr__(self):
        return 'EvoNormS02D(%d,%d)' % (self.in_chans, self.groups)


class ConditionalAffineCoupling(nn.Module):
    def __init__(self, layer, retained_chan, modified_chan=None):
        super().__init__()
        self.retained_chan = retained_chan
        if modified_chan is None:
            self.modified_chan = retained_chan
        else:
            self.modified_chan = modified_chan
        self.layer = layer

    def forward(self, x, cond):

        x_first = x[:, :self.retained_chan, :, :]
        x_second = x[:, self.retained_chan:, :, :]
        layer_out = self.layer(x_first, cond)
        scale = layer_out[:, :self.modified_chan, :, :]
        shift = layer_out[:, self.modified_chan:, :, :]
        y_first = x_first
        y_second = x_second*torch.exp(scale) + shift
        log_det = scale.sum(1).sum(1).sum(1)
        return (torch.cat([y_first, y_second], 1), log_det)

    def reverse(self, x, cond):
        x_first = x[:, :self.retained_chan, :, :]
        x_second = x[:, self.retained_chan:, :, :]
        layer_out = self.layer(x_first, cond)
        scale = layer_out[:, :self.modified_chan, :, :]
        shift = layer_out[:, self.modified_chan:, :, :]
        y_first = x_first
        y_second = (x_second-shift)*torch.exp(-scale)
        log_det = -scale.sum(1).sum(1).sum(1)
        return (torch.cat([y_first, y_second], 1), log_det)

    def init_with_data_cond(self, x, cond):
        self.layer.init_with_data_cond(x, cond)
       # pass


class ConditionalWrapper(nn.Module):
    def __init__(self, layer):
        super().__init__()
        self.layer = layer

    def forward(self, x, _cond):
        return self.layer(x)

    def init_with_data_cond(self, x, _cond):
        self.layer.init_with_data(x)

    def __repr__(self):
        return self.layer.__repr__()


def AffineCoupling(layer, retained_chan, modified_chan=None):
    return ConditionalAffineCoupling(ConditionalWrapper(layer), retained_chan, modified_chan)


class Inv1x1Conv(nn.Module):
    def __init__(self, num_chans):
        super(Inv1x1Conv, self).__init__()
        self.weight = nn.Parameter(torch.empty((num_chans, num_chans)))

    def forward(self, x, _cond):
        y = F.conv2d(x, self.weight.unsqueeze(-1).unsqueeze(-1))
        y_det = x.shape[2]*x.shape[3] * \
            torch.log(torch.abs(torch.det(self.weight))+1e-13)
        return (y, y_det)

    def reverse(self, x, _cond):
        weight_inv = torch.inverse(self.weight)
        y = F.conv2d(x, weight_inv.unsqueeze(-1).unsqueeze(-1))
        y_det = x.shape[2]*x.shape[3] * \
            torch.log(torch.abs(torch.det(weight_inv))+1e-13)
        return (y, y_det)

    def init_with_data_cond(self, x, _cond):
        self.weight.data = torch.tensor(scipy.stats.special_ortho_group.rvs(
            self.weight.data.shape[0])).float().to(x.device)

    def __repr__(self):
        return 'Inv1x1Conv(%d)' % self.weight.data.shape[0]


class ActNorm(nn.Module):
    def __init__(self, num_chans):
        super(ActNorm, self).__init__()
        self.scale = nn.Parameter(torch.empty((num_chans, 1, 1)))
        self.shift = nn.Parameter(torch.empty((num_chans, 1, 1)))

    def forward(self, x, cond):
        [b, _c, h, w] = x.shape
        y = (x+self.shift)*self.scale
        y_det = h*w*torch.sum(torch.log(torch.abs(self.scale)+1e-13)).expand(b)
        return (y, y_det)

    def reverse(self, x, cond):
        [b, _c, h, w] = x.shape
        y = x/self.scale - self.shift
        y_det = -h*w * \
            torch.sum(torch.log(torch.abs(self.scale)+1e-13)).expand(b)
        return (y, y_det)

    def init_with_data_cond(self, x, cond):
        [b, c, h, w] = x.shape
        self.shift.data = -x.mean(3).mean(2).mean(0).unsqueeze(1).unsqueeze(1)
        mean_zero = x + self.shift.data
        self.scale.data = torch.rsqrt(
            (mean_zero**2).sum(3).sum(2).sum(0).unsqueeze(1).unsqueeze(1)/(b*h*w-1))

    def __repr__(self):
        return 'ActNorm(%d)' % self.scale.data.shape[0]


class ReversibleFlow(nn.Module):
    def __init__(self, layers):
        super(ReversibleFlow, self).__init__()
        self.layers = nn.ModuleList(layers)

    def forward(self, x, cond):
        out = x
        out_det = 0
        for layer in self.layers:
            (y, y_det) = layer(out, cond)
            out = y
            out_det = out_det + y_det
        return (out, out_det)

    def reverse(self, x, cond):
        out = x
        out_det = 0
        for layer in self.layers[::-1]:
            (y, y_det) = layer.reverse(out, cond)
            out = y
            out_det = out_det+y_det
        return (out, out_det)

    def init_with_data_cond(self, x, cond):
        out = x
        for layer in self.layers:
            layer.init_with_data_cond(out, cond)
            (y, _y_det) = layer(out, cond)
            out = y


class ResidualBlock(nn.Module):
    def __init__(self, nf, gc=32, res_scale=0.2):
        super().__init__()

        def nonlinearity(in_chans):
            # return EvoNormS02D(
            #     in_chans, np.gcd(in_chans//2, 16))
            return nn.LeakyReLU()
        self.layer1 = nn.Sequential(
            nn.Conv2d(nf + 0 * gc, gc, 3, padding=1, bias=True, padding_mode='circular'), nonlinearity(gc))
        self.layer2 = nn.Sequential(
            nn.Conv2d(nf + 1 * gc, gc, 3, padding=1, bias=True, padding_mode='circular'), nonlinearity(gc))
        self.layer3 = nn.Sequential(
            nn.Conv2d(nf + 2 * gc, gc, 3, padding=1, bias=True, padding_mode='circular'), nonlinearity(gc))
        self.layer4 = nn.Sequential(
            nn.Conv2d(nf + 3 * gc, gc, 3, padding=1, bias=True, padding_mode='circular'), nonlinearity(gc))
        self.layer5 = nn.Sequential(
            nn.Conv2d(nf + 4 * gc, nf, 3, padding=1, bias=True, padding_mode='circular'), nonlinearity(nf))

        self.res_scale = res_scale

    def forward(self, x):
        layer1 = self.layer1(x)
        layer2 = self.layer2(torch.cat((x, layer1), 1))
        layer3 = self.layer3(torch.cat((x, layer1, layer2), 1))
        layer4 = self.layer4(torch.cat((x, layer1, layer2, layer3), 1))
        layer5 = self.layer5(torch.cat((x, layer1, layer2, layer3, layer4), 1))
        return layer5.mul(self.res_scale) + x

    def init_with_data_cond(self, _x, _cond):
        nn.init.kaiming_normal_(self.layer1[0].weight, a=1e-2)
        nn.init.normal_(self.layer1[0].bias, std=.02)
        nn.init.kaiming_normal_(self.layer2[0].weight, a=1e-2)
        nn.init.normal_(self.layer2[0].bias, std=.02)
        nn.init.kaiming_normal_(self.layer3[0].weight, a=1e-2)
        nn.init.normal_(self.layer3[0].bias, std=.02)
        nn.init.kaiming_normal_(self.layer4[0].weight, a=1e-2)
        nn.init.normal_(self.layer4[0].bias, std=.02)
        nn.init.zeros_(self.layer5[0].weight)
        nn.init.zeros_(self.layer5[0].bias)


class AffineGroup(nn.Module):
    def __init__(self, retained_chan=4, modified_chan=5):
        super(AffineGroup, self).__init__()
        input_chan = retained_chan+9
#        self.encode = nn.Sequential(nn.Conv2d(input_chan, 4*input_chan, 4, stride=2, padding=1, padding_mode='circular'),
#                                    nn.LeakyReLU(inplace=True),
#                                    nn.Conv2d(4*input_chan, 16*input_chan, 4,
#                                              stride=2, padding=1, padding_mode='circular'),
#                                    nn.LeakyReLU(inplace=True))
        self.conv0 = nn.Sequential(
            nn.Conv2d(input_chan, 112, 3, padding=1, bias=True, padding_mode='circular'))
        self.encode = ResidualBlock(112)
#        self.decode = nn.Sequential(
#            nn.ConvTranspose2d(
#                16*input_chan, 4*input_chan, 4, stride=2, padding=1),
#            nn.LeakyReLU(inplace=True),
#            nn.ConvTranspose2d(4*input_chan, 2*modified_chan, 4, stride=2, padding=1))
        self.decode = ResidualBlock(112)
        self.conv1 = nn.Conv2d(112, 2*modified_chan,
                               kernel_size=3, padding=1, bias=True, padding_mode='circular')

    def forward(self, x, cond):
        (original, style_std, style_mean) = cond
        out = self.conv0(torch.cat([original, x], 1))
        out = self.encode(out)
        out = adain(out, style_std, style_mean)
        out = self.decode(out)
        out = self.conv1(out)
        return out

    def init_with_data_cond(self, x, cond):
        nn.init.kaiming_normal_(self.conv0[0].weight, a=1e-2)
        nn.init.normal_(self.conv0[0].bias, std=.02)
        self.encode.init_with_data_cond(x, cond)
        self.decode.init_with_data_cond(x, cond)
        nn.init.zeros_(self.conv1.weight)
        nn.init.zeros_(self.conv1.bias)


def block():
    return ReversibleFlow([ActNorm(9), ConditionalAffineCoupling(AffineGroup(4, 5), 4, 5), Inv1x1Conv(9)])
   # return ReversibleFlow([ActNorm(9), Inv1x1Conv(9)])


def adain(x, new_std, new_mean):
    [b, c, h, w] = x.shape
    old_mean = x.mean(3).mean(2).reshape(b, c, 1, 1)
    zero_mean = x - old_mean
    old_inv_std = torch.rsqrt(
        (zero_mean**2).sum(3).sum(2)/(h*w-1)+.000001).reshape(b, c, 1, 1)
    unit_std = zero_mean*old_inv_std
    return unit_std*new_mean + new_mean


class FlowModel(nn.Module):
    def __init__(self, dim=128):
        super().__init__()
        self.main = nn.ModuleList(
            [block(), block(), block()])
        style_encoders = {}
        style_encoders[128] = nn.Sequential(
            nn.Conv2d(9, 12, 4, 2, padding=1, padding_mode='circular'),  # ->32
            nn.LeakyReLU(),
            nn.Conv2d(12, 48, 4, 2, padding=1, padding_mode='circular'),  # 16
            nn.LeakyReLU(),
            nn.Conv2d(48, 11, 4, 2, padding=1, padding_mode='circular'),  # 8
            nn.LeakyReLU(),
        )
        style_encoders[64] = nn.Sequential(
            nn.Conv2d(9, 48, 4, 2, padding=1, padding_mode='circular'),  # 16
            nn.LeakyReLU(),
            nn.Conv2d(48, 11, 4, 2, padding=1, padding_mode='circular'),  # 8
            nn.LeakyReLU(),
        )

        style_encoders[32] = nn.Sequential(
            nn.Conv2d(9, 11, 4, 2, padding=1, padding_mode='circular'),  # 8
            nn.LeakyReLU(),
        )

        self.style_encoder = style_encoders[dim]
        self.block_encoding = nn.Sequential(
            nn.Conv2d(3, 4*9, 3, padding=1,
                      padding_mode='circular', bias=True),
            nn.LeakyReLU(),
            ResidualBlock(4*9),
            nn.Conv2d(4*9, 4*9, 3, padding=1, padding_mode='circular'),
            nn.LeakyReLU()
        )
        self.style_encoder_linear = nn.Linear(8*8*11, 112*3*2)

    def forward(self, x, original):
        [b, c, h, w] = original.shape
        block_encoding = self.block_encoding(original).reshape(b, 4, 9, h, w)
        style_1 = self.style_encoder(block_encoding[:, 3, :, :, :])
        style_out = self.style_encoder_linear(
            style_1.reshape(b, 8*8*11)).reshape(b, 3, 2, 112)
        out_main = x
        det_main = 0
        for (i, layer) in enumerate(self.main):
            style_std = style_out[:, i, 0, :].reshape(b, 112, 1, 1)
            style_mean = style_out[:, i, 1, :].reshape(b, 112, 1, 1)
            cond = (block_encoding[:, i, :, :, :], style_std, style_mean)
            (y, det_y) = layer(out_main, cond)
            out_main = y
            det_main = det_main + det_y
        return (out_main, det_main)

    def reverse(self, x, original):
        [b, c, h, w] = original.shape
        block_encoding = self.block_encoding(original).reshape(b, 4, 9, h, w)
        style_1 = self.style_encoder(block_encoding[:, 3, :, :, :])
        style_out = self.style_encoder_linear(
            style_1.reshape(b, 8*8*11)).reshape(b, 3, 2, 112)
        out_main = x
        det_main = 0
        for (i, layer) in list(enumerate(self.main))[::-1]:
            style_std = style_out[:, i, 0, :].reshape(b, 112, 1, 1)
            style_mean = style_out[:, i, 1, :].reshape(b, 112, 1, 1)
            cond = (block_encoding[:, i, :, :, :], style_std, style_mean)
            (y, det_y) = layer.reverse(out_main, cond)
            out_main = y
            det_main = det_main + det_y
        return (out_main, det_main)

    def init_with_data_and_orig(self, x, original):
        for parameter in self.style_encoder.parameters():
            rank = len(parameter.shape)
            if rank >= 2:
                nn.init.kaiming_normal_(parameter, 1e-2)
            else:
                nn.init.normal_(parameter, std=.02)
        nn.init.kaiming_normal_(self.style_encoder_linear.weight)
        nn.init.normal_(self.style_encoder_linear.bias, std=0.02)

        nn.init.kaiming_normal_(self.block_encoding[0].weight, a=1e-2)
        nn.init.normal_(self.block_encoding[0].bias, std=.02)
        nn.init.kaiming_normal_(self.block_encoding[3].weight, a=1e-2)
        nn.init.normal_(self.block_encoding[3].bias, std=.02)
        self.block_encoding[2].init_with_data_cond(None, None)

        [b, c, h, w] = original.shape

        block_encoding = self.block_encoding(original).reshape(b, 4, 9, h, w)

        style_1 = self.style_encoder(block_encoding[:, 3, :, :, :])
        style_out = self.style_encoder_linear(
            style_1.reshape(b, 8*8*11)).reshape(b, 3, 2, 112)
        out_main = x
        for (i, layer) in enumerate(self.main):
            style_std = style_out[:, i, 0, :].reshape(b, 112, 1, 1)
            style_mean = style_out[:, i, 1, :].reshape(b, 112, 1, 1)
            cond = (block_encoding[:, i, :, :, :], style_std, style_mean)
            layer.init_with_data_cond(out_main, cond)
            (y, _det_y) = layer(out_main, cond)
            out_main = y


def num_params(model):
    model_parameters = filter(lambda p: p.requires_grad, model.parameters())
    params = sum([np.prod(p.size()) for p in model_parameters])
    return params


if __name__ == '__main__':
    model = FlowModel(dim=64)
    original = torch.rand(1, 3, 32, 32)
    diffs = torch.rand(1, 9, 32, 32)
    model.init_with_data_and_orig(diffs, original)
    original = torch.rand(1, 3, 32, 32)
    diffs = torch.rand(1, 9, 32, 32)
    noise = model(diffs, original)
    diffs_restored = model.reverse(noise[0], original)
    print(noise[1], diffs_restored[1])
    print(torch.mean((diffs_restored[0]-diffs)))
    print(num_params(model))


def train(model, data, batch_size, epochs, device, callback, optimizer, wavelets_to_apply, scaler=None):
    print("Beginning training!")

    autograd_enabled = scaler != None

    for epoch in range(epochs):
        print("begin epoch,", epoch)
        for (i, data_batch) in enumerate(data):
            optimizer.zero_grad()

            device_batch = data_batch[0].to(device)
            dequantization_noise = torch.empty_like(device_batch)
            dequantization_noise.uniform_(-0.5/255, 0.5/255)
            dequantized = device_batch

            for wave_app in wavelets_to_apply[:-1]:
                dequantized = apply_wavelet_c(dequantized, wave_app)[
                    :, :, 0, :, :]

            processed = apply_wavelet_c(
                dequantized, wavelets_to_apply[-1])
            [b, c, a, h, w] = processed.shape
            processed_original = processed[:, :, 0, :, :]
            processed_details = processed[:, :, 1:, :, :].reshape(b, c*3, h, w)

            with amp.autocast(enabled=autograd_enabled):

                encoding = model(processed_details, processed_original)
                log_prob = (-(encoding[0])**2).sum(3).sum(2).sum(1)/2 + \
                    encoding[1] - np.log(2*np.pi)*(3*c*h*w)/2
                loss = -torch.mean(log_prob)/(c*h*w*3*np.log(2))
                if loss.isnan() or loss.isinf():
                    det = torch.mean(encoding[1])/(c*h*3*np.log(2))
                    print("bad loss!", loss, det)
                    continue
                log_prob_cond = torch.bitwise_or(
                    torch.isinf(log_prob), torch.isnan(log_prob))
                log_prob_normalized = torch.where(
                    log_prob_cond, torch.zeros_like(log_prob), log_prob)
                loss = -torch.mean(log_prob_normalized)/(c*h*w*3*np.log(2))

            if autograd_enabled:
                scaler.scale(loss).backward()

                scaler.step(optimizer)

                scaler.update()
            else:
                loss.backward()
                optimizer.step()

            callback(i, len(data), loss, model, epoch, epochs)
