import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as opt
import numpy as np
import torch.cuda.amp as amp
import functools


class RevLinear(nn.Module):
    def __init__(self, nc):
        super().__init__()
        weight = torch.empty((nc, nc))
        nn.init.orthogonal_(weight)
        # nn.init.kaiming_normal_(weight, a=0.2)
        self.weight = nn.Parameter(weight, requires_grad=True)
        bias = torch.empty((nc,))
        nn.init.zeros_(bias)
        # nn.init.uniform_(bias, -0.1, 0.1)
        self.bias = nn.Parameter(bias)

    def forward(self, x):
        [b, _c] = x.shape

        log_det = torch.slogdet(self.weight)[1].reshape(1,).expand(b)
        return (self.bias + x@self.weight, log_det)

    def reverse(self, x):
        [b, c] = x.shape
        inv_matrix = torch.inverse(self.weight)
        log_det = -torch.slogdet(self.weight)[1].reshape(1,).expand(b)
        return ((x-self.bias)@inv_matrix, log_det)


class RevPReLU(nn.Module):
    def __init__(self, nc, eps=1e-3):
        super().__init__()
        self.eps = eps
        weight = torch.empty((nc,))
        nn.init.ones_(weight)
        # nn.init.uniform_(weight, 0.1, 0.3)
        # weight = torch.sqrt(weight - eps)
        self.weight = nn.Parameter(weight)

    def forward(self, x):
        [b, c] = x.shape
        multiplier = (self.weight**2 + self.eps).reshape(1, c).expand(b, c)
        eigenvalues = torch.where(
            x < 0, multiplier, torch.ones_like(multiplier))
        log_det = torch.log(eigenvalues+1e-06).sum(1)
        out = torch.where(x < 0, multiplier*x, x)
        return(out, log_det)

    def reverse(self, x):
        [b, c] = x.shape
        multiplier = (1/(self.weight**2 + self.eps)).reshape(1, c).expand(b, c)
        eigenvalues = torch.where(
            x < 0, multiplier, torch.ones_like(multiplier))
        log_det = torch.log(eigenvalues).sum(1)
        out = torch.where(x < 0, multiplier*x, x)
        return(out, log_det)


def logit(x):
    return torch.log(x/(1-x))


class Flow(nn.Module):
    def __init__(self, num_layers, dim=4):
        super().__init__()
        self.dim = 4
        self.nc = dim*dim*3
        network = []
        for _i in range(num_layers):
            network.append(RevLinear(self.nc))
            network.append(RevPReLU(self.nc))
        self.main = nn.ModuleList(network)

    def forward(self, img):
        [b, c, h, w] = img.shape
        out = img.reshape(b, c*h*w)
        log_det = torch.zeros(b).to(img.device)
        for layer in self.main:
            (y, det_y) = layer(out)
            out = y
            log_det += det_y
       # log_det += (out - 2*F.softplus(out)).sum(1)
       # out = torch.sigmoid(out)
        return (out, log_det)

    def reverse(self, noise):
        [b, c] = noise.shape
        out = noise
        #out = logit(noise)
        #log_det = -(out - 2*F.softplus(out)).sum(1)
        log_det = torch.zeros(b).to(out.device)
        for layer in list(self.main)[::-1]:
            (y, det_y) = layer.reverse(out)
            out = y
            log_det += det_y
        return (out.reshape(b, 3, self.dim, self.dim), log_det)


def num_params(model):
    model_parameters = filter(lambda p: p.requires_grad, model.parameters())
    params = sum([np.prod(p.size()) for p in model_parameters])
    return params


def likelyhood_function(x, dims):
    return torch.sum(-x**2, 1)/2 - dims * (np.log(2)+np.log(np.pi))/2
    #out = torch.where(x < 0, 500*x, torch.zeros_like(x))
    #out = torch.where(x > 1, -500*(x-1), out)
    # return torch.sum(out, 1)


if __name__ == "__main__":
    flow = Flow(10)
    img = torch.rand((8, 3, 4, 4))
    print(flow(img)[1].shape)
    print(likelyhood_function(torch.tensor([[2]]), 0))


def train(model, data, batch_size, epochs, device, callback, optimizer, scaler=None):
    print("Beginning training!")

    autograd_enabled = scaler != None

    for epoch in range(epochs):
        print("begin epoch,", epoch)
        for (i, data_batch) in enumerate(data):
            optimizer.zero_grad()

            device_batch = data_batch[0].to(device)

            [b, c, h, w] = device_batch.shape
            dims = c * h * w
            output = model(device_batch)
            log_likelyhood = likelyhood_function(output[0], dims) + output[1]
            loss = -torch.mean(log_likelyhood)/(dims*np.log(2))
            if loss.isinf() or loss.isnan():
                continue
            loss.backward()
            optimizer.step()
            callback(i, len(data), model, epoch, epochs, loss)
