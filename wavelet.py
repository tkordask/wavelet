import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as opt
import numpy as np
import scipy.stats as stats
from itertools import cycle
import pywt


def prepare_wavelet(name):
    wavelet = pywt.Wavelet(name)
    filter_length = len(wavelet.filter_bank[0])
    wavelet_kernel = torch.tensor(
        [wavelet.filter_bank[0], wavelet.filter_bank[1]]).reshape(2, filter_length)
    twoD_kernel = torch.einsum('ab,cd->acbd', wavelet_kernel,
                               wavelet_kernel).reshape(4, 1, filter_length, filter_length)
    return twoD_kernel


def apply_wavelet(img, wavelet):
    length = wavelet.shape[3]
    padding = length//2 - 1
    padded = F.pad(img, (padding, padding, padding, padding), mode='circular')
    return F.conv2d(padded, wavelet, stride=2)


def inverse_wavelet(img, wavelet):
    length = wavelet.shape[3]
    padding = length//2 - 1
    unfiltered = F.conv_transpose2d(img, wavelet, stride=2)
    if padding == 0:
        return unfiltered
    # return unfiltered
    y_len = img.shape[2]
    y_center = unfiltered[:, :, padding:-padding, :]
    up_shave = unfiltered[:, :, :padding, :].clone()
    down_shave = unfiltered[:, :, -padding:, :].clone()
    y_center[:, :, -padding:, :] += up_shave
    y_center[:, :, :padding, :] += down_shave

    x_len = y_center.shape[3]
    x_center = y_center[:, :, :, padding:-padding]
    left_shave = y_center[:, :, :, :padding].clone()
    right_shave = y_center[:, :, :, -padding:].clone()
    x_center[:, :, :, -padding:] += left_shave
    x_center[:, :, :, :padding] += right_shave
    return x_center


def apply_wavelet_c(img, wavelet):
    [b, c, h, w] = img.shape
    reshaped = img.reshape(b*c, 1, h, w)
    return apply_wavelet(reshaped, wavelet).reshape(b, c, 4, h//2, w//2)


def inverse_wavelet_c(img, wavelet):
    [b, c, a, h, w] = img.shape
    reshaped = img.reshape(b*c, a, h, w)
    return inverse_wavelet(reshaped, wavelet).reshape(b, c, h*2, w*2)


def find_best_wavelet(wavelet_dict, wavelets, wavelets_to_apply, data, batch_size, num_iters, device, callback, avg_img=None):
    data_iter = iter(data)
    for wv in wavelets:
        wavelet = prepare_wavelet(wv).to(device)
        relative_energy = 0

        batch = None
        i = 0
        while i < num_iters:
            for batch in data_iter:
                if i == num_iters:
                    break
                device_batch = batch[0].to(device)

                if avg_img != None:
                    device_batch = device_batch - avg_img

                for wave_app in wavelets_to_apply:
                    device_batch = apply_wavelet_c(device_batch, wave_app)[
                        :, :, 0, :, :]

                transformed = apply_wavelet_c(device_batch, wavelet)
                energy_low = (transformed[:, :, 0, :, :]
                              ** 2).sum(1).sum(1).sum(1)
                energy_high = (transformed[:, :, 1:, :, :]
                               ** 2).sum(1).sum(1).sum(1).sum(1)
                relative_energy += torch.mean(energy_high/energy_low)

                callback(wv, relative_energy, i, num_iters, wavelet_dict)
                i += 1
            if i != num_iters:
                data_iter = iter(data)

        wavelet_dict[wv] = relative_energy/num_iters
    return wavelet_dict


def make_avg_img(data, device):
    avg_img = None
    total_images = 0
    for batch in data:
        device_batch = batch[0].to(device)
        if avg_img == None:
            avg_img = device_batch.sum(0)
        else:
            avg_img += device_batch.sum(0)
        total_images += device_batch.shape[0]
    return avg_img/total_images


test = torch.tensor([[1, 2, 3, 4, 5, 6], [7, 8, 9, 10, 11, 12], [1, 2, 3, 4, 5, 6], [
    7, 8, 9, 10, 11, 12], [1, 2, 3, 4, 5, 6], [7, 8, 9, 10, 11, 12]]).reshape(1, 1, 6, 6).float()

# test.normal_()

# test = torch.rand((1, 3, 4, 4))

# wavelet = prepare_wavelet('haar')

# print(test)
# x = apply_wavelet_c(test, wavelet)
# print(inverse_wavelet_c(x, wavelet))

# prepare_wavelet('db2')
