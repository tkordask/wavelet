import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as opt
import numpy as np
import scipy.stats
import pywt


def prepare_wavelet(name):
    wavelet = pywt.Wavelet(name)
    filter_length = len(wavelet.filter_bank[0])
    wavelet_kernel = torch.tensor(
        [wavelet.filter_bank[0], wavelet.filter_bank[1]]).reshape(2, filter_length)
    twoD_kernel = torch.einsum('ab,cd->acbd', wavelet_kernel,
                               wavelet_kernel).reshape(4, 1, filter_length, filter_length)
    return twoD_kernel


def apply_wavelet(img, wavelet):
    length = wavelet.shape[3]
    padding = length//2 - 1
    padded = F.pad(img, (padding, padding, padding, padding), mode='circular')
    return F.conv2d(padded, wavelet, stride=2)


def inverse_wavelet(img, wavelet):
    length = wavelet.shape[3]
    padding = length//2 - 1
    unfiltered = F.conv_transpose2d(img, wavelet, stride=2)
    if padding == 0:
        return unfiltered
    # return unfiltered
    y_len = img.shape[2]
    y_center = unfiltered[:, :, padding:-padding, :]
    up_shave = unfiltered[:, :, :padding, :].clone()
    down_shave = unfiltered[:, :, -padding:, :].clone()
    y_center[:, :, -padding:, :] += up_shave
    y_center[:, :, :padding, :] += down_shave

    x_len = y_center.shape[3]
    x_center = y_center[:, :, :, padding:-padding]
    left_shave = y_center[:, :, :, :padding].clone()
    right_shave = y_center[:, :, :, -padding:].clone()
    x_center[:, :, :, -padding:] += left_shave
    x_center[:, :, :, :padding] += right_shave
    return x_center


def apply_wavelet_c(img, wavelet):
    [b, c, h, w] = img.shape
    reshaped = img.reshape(b*c, 1, h, w)
    return apply_wavelet(reshaped, wavelet).reshape(b, c, 4, h//2, w//2)


def inverse_wavelet_c(img, wavelet):
    [b, c, a, h, w] = img.shape
    reshaped = img.reshape(b*c, a, h, w)
    return inverse_wavelet(reshaped, wavelet).reshape(b, c, h*2, w*2)


class GBlock(nn.Module):
    def __init__(self, chan_in, chan_out, downscale, upscale, bn=True):
        super().__init__()
        self.conv0 = nn.utils.spectral_norm(
            nn.Conv2d(chan_in, chan_in, 3, padding=1, bias=not bn, padding_mode='circular'))
        self.conv1 = nn.utils.spectral_norm(
            nn.Conv2d(chan_in, chan_out, 3, padding=1, bias=not bn, padding_mode='circular'))

        self.upscale = upscale
        self.downscale = downscale

        self.activation = F.relu

        self.bn = bn
        if self.bn:
            #self.bn_layer0 = nn.BatchNorm2d(chan_in)
            #self.bn_layer1 = nn.BatchNorm2d(chan_out)
            self.bn_layer0 = nn.InstanceNorm2d(chan_in, affine=True)
            self.bn_layer1 = nn.InstanceNorm2d(chan_out, affine=True)

    def forward(self, x):
        out = self.conv0(x)
        if self.bn:
            out = self.bn_layer0(out)
        out = self.activation(out)
        out = self.conv1(out)
        if self.bn:
            out = self.bn_layer1(out)
        out = self.activation(out)
        if self.upscale:
            out = F.interpolate(out, scale_factor=2, mode='bilinear')
        if self.downscale:
            out = F.avg_pool2d(out, 2)
        return out

    def init_weights(self):
        nn.init.orthogonal_(self.conv0.weight)
        if not self.bn:
            nn.init.normal_(self.conv0.bias, std=.02)
        nn.init.orthogonal_(self.conv1.weight)
        if not self.bn:
            nn.init.normal_(self.conv1.bias, std=.02)


class Generator(nn.Module):
    def __init__(self):
        super().__init__()
        self.downscale = nn.Sequential(
            GBlock(12, 4*12, downscale=True, upscale=False),
            GBlock(4*12, 16*12, downscale=True, upscale=False),
            GBlock(16*12, 32*12, downscale=True, upscale=False),
        )
        self.upscale = nn.Sequential(
            GBlock(32*12, 16*9, downscale=False, upscale=True),
            GBlock(16*9, 4*9, downscale=False, upscale=True),
            GBlock(4*9, 9, downscale=False, upscale=True),
        )
        self.final = nn.Sequential(

            nn.Conv2d(9, 9, 3, padding=1, padding_mode='circular'),
            nn.Tanh())

    def forward(self, x):
        out = self.downscale(x)
        out = self.upscale(out)
        out = self.final(out)
        return out

    def init_weights(self):
        for l in self.downscale:
            l.init_weights()
        for l in self.upscale:
            l.init_weights()
        nn.init.orthogonal_(self.final[0].weight)
        nn.init.normal_(self.final[0].bias)


class Discriminator(nn.Module):
    def __init__(self):
        super().__init__()
        self.downscale = nn.Sequential(
            GBlock(12, 4*12, downscale=True, upscale=False),  # 32x32
            GBlock(4*12, 16*12, downscale=True, upscale=False),  # 16x16
            GBlock(16*12, 32*12, downscale=True, upscale=False),  # 8x8
            GBlock(32*12, 8*12, downscale=True, upscale=False),  # 4x4
        )
        self.linear = nn.utils.spectral_norm(nn.Linear(4*4*8*12, 1))

    def forward(self, x):
        out = self.downscale(x).reshape((-1, 4*4*8*12))
        out = self.linear(out)
        return out

    def init_weights(self):
        for l in self.downscale:
            l.init_weights()
        nn.init.orthogonal_(self.linear.weight)


def num_params(model):
    model_parameters = filter(lambda p: p.requires_grad, model.parameters())
    params = sum([np.prod(p.size()) for p in model_parameters])
    return params


# model = Generator()
# model.init_weights()
# x = torch.rand(1, 12, 128, 128)
# print(model(x).shape)
# print(num_params(model))

def train_l1(model, data, batch_size, epochs, device, callback, optimizer, wavelet):
    print("Beginning training!")
    scale_factor = torch.abs(wavelet).sum(3).sum(2).sum(1)[1:].reshape(
        1, 3, 1, 1).expand(3, 3, 1, 1).reshape(1, 9, 1, 1)
    for epoch in range(epochs):
        print("begin epoch,", epoch)
        for (i, data_batch) in enumerate(data):
            optimizer.zero_grad()
            model.train()

            device_batch = data_batch[0].to(device)

            processed = apply_wavelet_c(
                device_batch, wavelet)
            [b, c, _a, h, w] = processed.shape
            processed_original = processed[:, :, 0, :, :]
            processed_details = processed[:, :, 1:, :, :].reshape(b, c*3, h, w)
            noise = torch.randn((b, 9, h, w)).to(device)
            generated_details = model(
                torch.cat([processed_original, noise], 1))*scale_factor

            mse = torch.mean(
                torch.abs((generated_details-processed_details)).sum(3).sum(2).sum(1))

            mse.backward()

            optimizer.step()

            callback(i, len(data), mse, model, epoch, epochs)


def train_gan(gen, disc, data, batch_size, epochs, device, callback, optimizer_gen, optimizer_disc, wavelet, loss_scale=1):
    print("Beginning training!")
    scale_factor = torch.abs(wavelet).sum(3).sum(2).sum(1)[1:].reshape(
        1, 3, 1, 1).expand(3, 3, 1, 1).reshape(1, 9, 1, 1)
    for epoch in range(epochs):
        print("begin epoch,", epoch)
        for (i, data_batch) in enumerate(data):
            optimizer_gen.zero_grad()
            optimizer_disc.zero_grad()
            disc.train()
            gen.eval()

            device_batch = data_batch[0].to(device)

            processed = apply_wavelet_c(
                device_batch, wavelet)
            [b, c, _a, h, w] = processed.shape
            processed_original = processed[:, :, 0, :, :]
            processed_details = processed[:, :, 1:, :, :].reshape(b, c*3, h, w)
            noise = torch.randn((b, 9, h, w)).to(device)
            with torch.no_grad():
                generated_details = gen(
                    torch.cat([processed_original, noise], 1))*scale_factor

            generated_img = torch.cat(
                [processed_original, generated_details], 1)
            original_img = torch.cat(
                [processed_original, processed_details], 1)
            loss_disc = torch.mean(F.relu(1-disc(generated_img)) +
                                   F.relu(1+disc(original_img)))
            loss_disc.backward()
            optimizer_disc.step()

            optimizer_disc.zero_grad()
            optimizer_gen.zero_grad()
            disc.eval()
            gen.train()

            noise = torch.randn((b, 9, h, w)).to(device)
            generated_details = gen(
                torch.cat([processed_original, noise], 1))*scale_factor

            generated_img = torch.cat(
                [processed_original, generated_details], 1)

            l1 = torch.mean(
                torch.abs((generated_details-processed_details)).sum(3).sum(2).sum(1))

            loss_gen = torch.mean(disc(generated_img)) + loss_scale*l1
            loss_gen.backward()
            optimizer_gen.step()
            callback(i, len(data), loss_gen, loss_disc,
                     gen, disc, epoch, epochs)
