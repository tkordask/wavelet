import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as opt
import numpy as np
import scipy.stats as stats


class Multiresolution(nn.Module):
    def __init__(self, n=2):
        super(Multiresolution, self).__init__()
        self.n = n
        # init_param = torch.eye(12).reshape(12, 3, 2, 2)
        init_param = torch.zeros((12, 12)).reshape(12, 3, 2, 2)

        self.w128to64 = nn.parameter.Parameter(
            # torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
            torch.randn((12, 3, n, n)))
        self.w64to32 = nn.parameter.Parameter(
            # torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
            torch.randn((12, 3, n, n)))
        self.w32to16 = nn.parameter.Parameter(
            #            torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
            torch.randn((12, 3, n, n)))
        self.w16to8 = nn.parameter.Parameter(
            #            torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
            torch.randn((12, 3, n, n)))
        self.w8to4 = nn.parameter.Parameter(
            #            torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
            torch.randn((12, 3, n, n)))
        self.w4to2 = nn.parameter.Parameter(
            #            torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
            torch.randn((12, 3, n, n)))
        self.w2to1 = nn.parameter.Parameter(
            #            torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
            torch.randn((12, 3, n, n)))
        for parameter in self.parameters():
            #            print(parameter.size())
            nn.init.kaiming_uniform_(parameter, a=1, nonlinearity='leaky_relu')

    def forward(self, x):
        def conv(x, w):
            return F.conv2d(x, w, stride=2, padding=self.n//2 - 1)

        x_64 = conv(x, self.w128to64)
        x_32 = conv(x_64[:, :3, :, :], self.w64to32)
        x_16 = conv(x_32[:, :3, :, :], self.w32to16)
        x_8 = conv(x_16[:, :3, :, :], self.w16to8)
        x_4 = conv(x_8[:, :3, :, :], self.w8to4)
        x_2 = conv(x_4[:, :3, :, :], self.w4to2)
        x_1 = conv(x_2[:, :3, :, :], self.w2to1)

        return(x_1, x_2[:, 3:, :, :], x_4[:, 3:, :, :], x_8[:, 3:, :, :], x_16[:, 3:, :, :], x_32[:, 3:, :, :], x_64[:, 3:, :, :])

    def reverse(self, x):
        def rev_conv(x, w):
            return F.conv_transpose2d(x, w, stride=2, padding=self.n//2-1)
        x_1 = x[0]
        x_2 = torch.cat(
            [rev_conv(x_1, self.w2to1), x[1]], 1)
        x_4 = torch.cat(
            [rev_conv(x_2, self.w4to2), x[2]], 1)
        x_8 = torch.cat(
            [rev_conv(x_4, self.w8to4), x[3]], 1)
        x_16 = torch.cat(
            [rev_conv(x_8, self.w16to8), x[4]], 1)
        x_32 = torch.cat(
            [rev_conv(x_16, self.w32to16), x[5]], 1)
        x_64 = torch.cat(
            [rev_conv(x_32, self.w64to32), x[6]], 1)
        x_128 = rev_conv(x_64, self.w128to64)
        return x_128


class MultiresolutionMatInv(nn.Module):
    def __init__(self):
        super(MultiresolutionMatInv, self).__init__()

        # init_param = torch.eye(12).reshape(12, 3, 2, 2)
        init_param = torch.zeros((12, 12)).reshape(12, 3, 2, 2)

        self.w128to64 = nn.parameter.Parameter(
            #    torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
            torch.empty((12, 3, 2, 2)).uniform_(-1, 1))
        self.w64to32 = nn.parameter.Parameter(
            #    torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
            torch.empty((12, 3, 2, 2)).uniform_(-1, 1))
        self.w32to16 = nn.parameter.Parameter(
            #    torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
            torch.empty((12, 3, 2, 2)).uniform_(-1, 1))
        self.w16to8 = nn.parameter.Parameter(
            #    torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
            torch.empty((12, 3, 2, 2)).uniform_(-1, 1))
        self.w8to4 = nn.parameter.Parameter(
            #    torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
            torch.empty((12, 3, 2, 2)).uniform_(-1, 1))
        self.w4to2 = nn.parameter.Parameter(
            #    torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
            torch.empty((12, 3, 2, 2)).uniform_(-1, 1))
        self.w2to1 = nn.parameter.Parameter(
            #    torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
            torch.empty((12, 3, 2, 2)).uniform_(-1, 1))

    def forward(self, x):
        x_64 = F.conv2d(x, self.w128to64, stride=2)
        x_32 = F.conv2d(x_64[:, :3, :, :], self.w64to32, stride=2)
        x_16 = F.conv2d(x_32[:, :3, :, :], self.w32to16, stride=2)
        x_8 = F.conv2d(x_16[:, :3, :, :], self.w16to8, stride=2)
        x_4 = F.conv2d(x_8[:, :3, :, :], self.w8to4, stride=2)
        x_2 = F.conv2d(x_4[:, :3, :, :], self.w4to2, stride=2)
        x_1 = F.conv2d(x_2[:, :3, :, :], self.w2to1, stride=2)

        return(x_1, x_2[:, 3:, :, :], x_4[:, 3:, :, :], x_8[:, 3:, :, :], x_16[:, 3:, :, :], x_32[:, 3:, :, :], x_64[:, 3:, :, :])

    def reverse(self, x):
        def rev_weight(w):
            w_matrix = w.reshape(12, 12)
            w_inv = torch.inverse(w_matrix)
            w_inv_transpose = torch.transpose(w_inv, 0, 1)
            return w_inv_transpose.reshape(12, 3, 2, 2)

        x_1 = x[0]
        x_2 = torch.cat(
            [F.conv_transpose2d(x_1, rev_weight(self.w2to1), stride=2), x[1]], 1)
        x_4 = torch.cat(
            [F.conv_transpose2d(x_2, rev_weight(self.w4to2), stride=2), x[2]], 1)
        x_8 = torch.cat(
            [F.conv_transpose2d(x_4, rev_weight(self.w8to4), stride=2), x[3]], 1)
        x_16 = torch.cat(
            [F.conv_transpose2d(x_8, rev_weight(self.w16to8), stride=2), x[4]], 1)
        x_32 = torch.cat(
            [F.conv_transpose2d(x_16, rev_weight(self.w32to16), stride=2), x[5]], 1)
        x_64 = torch.cat(
            [F.conv_transpose2d(x_32, rev_weight(self.w64to32), stride=2), x[6]], 1)
        x_128 = F.conv_transpose2d(x_64, rev_weight(self.w128to64), stride=2)
        return x_128


class Multiresolution2(nn.Module):
    def __init__(self, n=2, noise=0):
        super(Multiresolution2, self).__init__()
        self.n = n
        # init_param = torch.eye(12).reshape(12, 3, 2, 2)
        init_param = torch.zeros((12, 12)).reshape(12, 3, 2, 2)

        self.w128to64 = nn.parameter.Parameter(
            # torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
            torch.zeros((12, 3, n, n)))
        self.w64to32 = nn.parameter.Parameter(
            # torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
            torch.zeros((12, 3, n, n)))
        self.w32to16 = nn.parameter.Parameter(
            #            torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
            torch.zeros((12, 3, n, n)))
        self.w16to8 = nn.parameter.Parameter(
            #            torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
            torch.zeros((12, 3, n, n)))
       # self.w8to4 = nn.parameter.Parameter(
        #            torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
       #     torch.randn((12, 3, n, n)))
       # self.w4to2 = nn.parameter.Parameter(
        #            torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
      #      torch.randn((12, 3, n, n)))
      #  self.w2to1 = nn.parameter.Parameter(
        #            torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
      #      torch.randn((12, 3, n, n)))
        self.w8to4 = nn.Linear(192, 192, bias=False)
        self.w4to2 = nn.Linear(48, 48, bias=False)
        self.w2to1 = nn.Linear(12, 12, bias=False)

        self.w64to128 = nn.parameter.Parameter(
            # torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
            torch.zeros((12, 3, n, n)))
        self.w32to64 = nn.parameter.Parameter(
            # torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
            torch.zeros((12, 3, n, n)))
        self.w16to32 = nn.parameter.Parameter(
            #            torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
            torch.zeros((12, 3, n, n)))
        self.w8to16 = nn.parameter.Parameter(
            #            torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
            torch.zeros((12, 3, n, n)))
       # self.w4to8 = nn.parameter.Parameter(
        #            torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
       #     torch.randn((12, 3, n, n)))
       # self.w2to4 = nn.parameter.Parameter(
        #            torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
       #     torch.randn((12, 3, n, n)))
       # self.w1to2 = nn.parameter.Parameter(
        #            torch.tensor(stats.ortho_group.rvs(12)).reshape(12, 3, 2, 2).float())
       #     torch.randn((12, 3, n, n)))

        self.w4to8 = nn.Linear(192, 192, bias=False)
        self.w2to4 = nn.Linear(48, 48, bias=False)
        self.w1to2 = nn.Linear(12, 12, bias=False)

        haar = torch.tensor([[[1, 1], [1, 1]], [[1, -1], [1, -1]],
                             [[1, 1], [-1, -1]], [[1, -1], [-1, 1]]])/2.0
        haar_t = torch.zeros((12, 3, 2, 2))
        # print(haar.shape)
        for color in range(3):
            for i in range(4):
                haar_t[color+3*i, color, :, :] = haar[i]

        haar_padded = F.pad(haar_t, (n//2-1, n//2-1, n//2-1, n//2-1))

        for parameter in self.parameters():
            if parameter.shape == (12, 3, n, n):
                #                new_param = torch.zeros((4, n, n))
                #                nn.init.kaiming_uniform_(
                #                    new_param, a=1, nonlinearity='leaky_relu')
                #                for color in range(3):
                #                    for i in range(4):
                #                        parameter.data[color+3*i, color,
                #                                       :, :] = new_param[i, :, :]
                if parameter.shape == (12, 3, n, n):
                    parameter.data = haar_padded + \
                        torch.randn_like(parameter.data)*noise

        self.w8to4.weight.data = torch.tensor(
            stats.ortho_group.rvs(192)).float()
        self.w4to8.weight.data = torch.t(self.w8to4.weight.clone())
        self.w4to2.weight.data = torch.tensor(
            stats.ortho_group.rvs(48)).float()
        self.w2to4.weight.data = torch.t(self.w4to2.weight.clone())
        self.w2to1.weight.data = torch.tensor(
            stats.ortho_group.rvs(12)).float()
        self.w1to2.weight.data = torch.t(self.w2to1.weight.clone())

    def forward(self, x):
        def conv(x, w):
            padding = self.n//2-1
            in_padded = F.pad(
                x, (padding, padding, padding, padding), mode='reflect')
            return F.conv2d(in_padded, w, stride=2)

        def run_lin(x, layer):
            [b, c, w, h] = x.shape
            rehsaped_in = x.reshape((b, c*w*h))
            out = layer(rehsaped_in)
            return out.reshape((b, 4*c, w//2, h//2))

        x_64 = conv(x, self.w128to64)
        x_32 = conv(x_64[:, :3, :, :], self.w64to32)
        x_16 = conv(x_32[:, :3, :, :], self.w32to16)
        x_8 = conv(x_16[:, :3, :, :], self.w16to8)
      #  x_4 = conv(x_8[:, :3, :, :], self.w8to4)
      #  x_2 = conv(x_4[:, :3, :, :], self.w4to2)
      #  x_1 = conv(x_2[:, :3, :, :], self.w2to1)
        x_4 = run_lin(x_8[:, :3, :, :], self.w8to4)
        x_2 = run_lin(x_4[:, :3, :, :], self.w4to2)
        x_1 = run_lin(x_2[:, :3, :, :], self.w2to1)

        return(x_1, x_2[:, 3:, :, :], x_4[:, 3:, :, :], x_8[:, 3:, :, :], x_16[:, 3:, :, :], x_32[:, 3:, :, :], x_64[:, 3:, :, :])

    def reverse(self, x):
        def rev_conv(x, w):
            return F.conv_transpose2d(x, w, stride=2, padding=self.n//2-1)

        def run_lin(x, layer):
            [b, c, w, h] = x.shape
            rehsaped_in = x.reshape((b, c*w*h))
            out = layer(rehsaped_in)
            return out.reshape((b, c//4, w*2, h*2))
        x_1 = x[0]
        x_2 = torch.cat(
            [run_lin(x_1, self.w1to2), x[1]], 1)
        x_4 = torch.cat(
            [run_lin(x_2, self.w2to4), x[2]], 1)
        x_8 = torch.cat(
            [run_lin(x_4, self.w4to8), x[3]], 1)
        x_16 = torch.cat(
            [rev_conv(x_8, self.w8to16), x[4]], 1)
        x_32 = torch.cat(
            [rev_conv(x_16, self.w16to32), x[5]], 1)
        x_64 = torch.cat(
            [rev_conv(x_32, self.w32to64), x[6]], 1)
        x_128 = rev_conv(x_64, self.w64to128)
        return x_128


def orthogonality_loss(weights):

    eye = torch.eye(12).to(weights.device)

    weights_as_matrix = weights.reshape(
        (12, weights.size()[1]*weights.size()[2]*weights.size()[3]))

    weights_tranpose = torch.transpose(weights_as_matrix, 0, 1)

    orthogonality = torch.matmul(weights_as_matrix, weights_tranpose)

    return torch.sqrt(torch.sum((orthogonality - eye)**2))


def orth_conv_loss(weights):
    self_conv = F.conv2d(weights, weights, stride=2, padding=1)
    eye = torch.eye(12).to(weights.device).reshape(
        (12, 12, 1, 1)).expand((12, 12, 2, 2))

    return torch.sqrt(torch.sum((self_conv-eye)**2))


def conv_orth_dist(kernel, stride=1):
    dev = kernel.device
    [o_c, i_c, w, h] = kernel.shape
    assert (w == h), "Do not support rectangular kernel"
    # half = np.floor(w/2)
    assert stride < w, "Please use matrix orthgonality instead"
    new_s = stride*(w-1) + w  # np.int(2*(half+np.floor(half/stride))+1)
    temp = torch.eye(new_s*new_s*i_c).reshape((new_s*new_s *
                                               i_c, i_c, new_s, new_s)).to(dev)
    out = (F.conv2d(temp, kernel, stride=stride)
           ).reshape((new_s*new_s*i_c, -1))
    Vmat = out[np.floor(new_s**2/2).astype(int)::new_s**2, :]
    temp = np.zeros((i_c, i_c*new_s**2))
    for i in range(temp.shape[0]):
        temp[i,
             np.floor(new_s**2/2).astype(int)+new_s**2*i] = 1
    return torch.norm(Vmat@torch.t(out) - torch.from_numpy(temp).float().to(dev))


def train(model, data, batch_size, epochs, device, callback, beta=1, lr=0.0001, p=1, eps=.001):
    print("Beginning training!")

    optimizer = opt.Adam(model.parameters(), lr)

    for epoch in range(epochs):
        print("begin epoch,", epoch)
        for (i, data_batch) in enumerate(data):
            optimizer.zero_grad()

            device_batch = data_batch[0].to(device)

            energy_loss = 0

            encoding = model(device_batch)

            total_energy_norm = torch.zeros(
                (device_batch.size()[0],)).to(device)

            for group in encoding:
                energies = torch.abs(group)
                energies_normed = torch.sqrt(energies+eps).sum(1).sum(1).sum(1)
                total_energy_norm += energies_normed

            energy_loss = torch.mean(total_energy_norm**(2), 0)

            # reconstruction = dec(encoding)

            # reconstruction_error = loss(device_batch, reconstruction)

            orthogonality = 0
            for weight in model.parameters():
                orthogonality += orthogonality_loss(weight)

            total_loss = energy_loss+beta*orthogonality

            total_loss.backward()
            optimizer.step()

            callback(i, len(data), orthogonality, energy_loss,
                     total_loss, model, epoch, epochs)


def train_mse(model, data, batch_size, epochs, device, callback, optimizer, reconstruction_multiplier=10):
    print("Beginning training!")

    for epoch in range(epochs):
        print("begin epoch,", epoch)
        for (i, data_batch) in enumerate(data):
            optimizer.zero_grad()

            device_batch = data_batch[0].to(device)

            energy_loss = 0

            encoding = model(device_batch)
            reconstructed_img = model.reverse(encoding)

            compressed_encodings = []

            for n in range(6):
                current_encoding = []
                for k in range(7):
                    if k > n:
                        current_encoding.append(0*encoding[k])
                    else:
                        current_encoding.append(encoding[k])
                compressed_encodings.append(current_encoding)
            swapped_encodings = list(zip(*compressed_encodings))
            concatted_encodings = list(map(
                lambda x: torch.cat(x, 0), swapped_encodings))
            reconstruction = model.reverse(concatted_encodings)
            repeated_originals = torch.cat(6*[device_batch], 0)
            reconstruction_loss = torch.mean(
                torch.abs(reconstruction-repeated_originals))
            orig_reconstruction_loss = reconstruction_multiplier * \
                torch.mean(
                    F.relu(torch.abs(device_batch-reconstructed_img) - 0.4/255))

            # energy_loss = torch.mean(total_energy_norm**(2), 0)

            # reconstruction = dec(encoding)

            # reconstruction_error = loss(device_batch, reconstruction)

            orthogonality = 0
    #        for weight in model.parameters():
    #            orthogonality += conv_orth_dist(weight)

            total_loss = reconstruction_loss+orig_reconstruction_loss

            total_loss.backward()
            optimizer.step()

            callback(i, len(data), orig_reconstruction_loss, reconstruction_loss,
                     total_loss, model, epoch, epochs)


x = torch.ones((1, 3, 128, 128))


mr = Multiresolution2(n=8)

# for param in mr.parameters():
#    print(conv_orth_dist(param, stride=2))

print(mr.reverse(mr(x)).size())
