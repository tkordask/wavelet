import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as opt
import torchdiffeq as tde
import numpy as np


class FFJORDProb(nn.Module):
    def __init__(self, diff_func):
        super().__init__()
        self.diff_func = diff_func
        self.diff_func.requires_grad_()

    def forward(self, t, x):
        b, n = x.shape
        with torch.enable_grad():
            input = torch.autograd.Variable(
                x[:, 1:].reshape([b]+self.shape), requires_grad=True)
            t_vector = t.float().expand_as(input[:, :1])
            out = self.diff_func(torch.cat([input, t_vector], 1))
        # out = self.diff_func(input)

        if self.trace_calculation == 'hutchinson':
            jvp = torch.autograd.grad(
                out, input, self.noise, retain_graph=True)[0]
            trace = torch.einsum('bchw,bchw->b', self.noise, jvp)
        else:
            trace = 0
            for i in range(n-1):
                trace += torch.autograd.grad(out.reshape(b, n-1)
                                             [:, i], input, torch.ones(b).to(x), retain_graph=True)[0].reshape(b, n-1)[:, i]
        b, c, h, w = out.shape
        return torch.cat([trace[:, None], out.reshape(b, c*h*w)], 1)


class FFJORD(nn.Module):
    def __init__(self, diff_func, noise_distribution="rademacher", trace_calculation='hutchinson'):
        super().__init__()
        self.prob = FFJORDProb(diff_func)
        self.noise_distribution = noise_distribution
        self.prob.trace_calculation = trace_calculation

    def forward(self, x):
        if self.noise_distribution == "normal":
            self.prob.noise = torch.empty_like(x)
            self.prob.noise.normal_()
        else:
            self.prob.noise = torch.empty_like(x)
            self.prob.noise.random_(0, 2)
            self.prob.noise = 2*self.prob.noise - 1
        b, c, h, w = x.shape
        self.prob.shape = [c, h, w]
        input = x.reshape(b, c*h*w)
        input = torch.cat([torch.zeros(b, 1).to(x), input], 1)
        out = tde.odeint_adjoint(self.prob, input,
                                 torch.linspace(0, 1, 2).to(input))[1]
        log_p = out[:, 0]
        z = out[:, 1:].reshape(b, c, h, w)
        return (z, log_p)

    def reverse(self, x):
        if self.noise_distribution == "normal":
            self.prob.noise = torch.empty_like(x)
            self.prob.noise.normal_()
        else:
            self.prob.noise = torch.empty_like(x)
            self.prob.noise.random_(0, 2)
            self.prob.noise = 2*self.prob.noise - 1
        b, c, h, w = x.shape
        self.prob.shape = [c, h, w]
        input = x.reshape(b, c*h*w)
        input = torch.cat([torch.zeros(b, 1).to(x), input], 1)
        out = tde.odeint_adjoint(self.prob, input,
                                 torch.linspace(1, 0, 2).to(input))[1]
        log_p = out[:, 0]
        z = out[:, 1:].reshape(b, c, h, w)
        return (z, log_p)


class ConvBlock(nn.Module):
    def __init__(self, nc):
        super().__init__()
        self.main = nn.Sequential(
            nn.Softplus(),
            nn.Conv2d(nc+1, nc, 3, padding=1),
            nn.Softplus(),
            nn.Conv2d(nc, nc, 3, padding=1),
            nn.Softplus(),
            nn.Conv2d(nc, nc, 3, padding=1),
            nn.Softplus(),
            nn.Conv2d(nc, nc, 3, padding=1)
        )
        for parameter in self.main.parameters():
            rank = len(parameter.shape)
            if rank >= 2:
                nn.init.kaiming_normal_(parameter, 0.2)
            else:
                nn.init.normal_(parameter, std=.02)
        nn.init.zeros_(self.main[-1].weight)
        nn.init.zeros_(self.main[-1].bias)

    def forward(self, x):
        return self.main(x)


class LinearBlock(nn.Module):
    def __init__(self, nc):
        super().__init__()
        self.main = nn.Sequential(
            nn.Flatten(),
            nn.Softplus(),
            nn.Linear(nc+1, nc),
            nn.Softplus(),
            nn.Linear(nc, nc),
            nn.Softplus(),
            nn.Linear(nc, nc),
            nn.Softplus(),
            nn.Linear(nc, nc),
            nn.Unflatten(1, (nc, 1, 1))
        )
        for parameter in self.main.parameters():
            rank = len(parameter.shape)
            if rank >= 2:
                nn.init.kaiming_normal_(parameter, 0.2)
            else:
                nn.init.normal_(parameter, std=.02)
        nn.init.zeros_(self.main[-2].weight)
        nn.init.zeros_(self.main[-2].bias)

    def forward(self, x):
        return self.main(x)


def logit(x):
    return -torch.log(1/x - 1)


class MultiscaleFlow(nn.Module):
    def __init__(self, dimension, num_squeezings, layers_per_squeeze):
        super().__init__()

        def squeeze_group(nc):
            layers = [FFJORD(ConvBlock(nc*2))]
            for i in range(1, layers_per_squeeze):
                layers.append(FFJORD(ConvBlock(nc)))
            return nn.ModuleList(layers)
        cur_features = 3*2
        squeezings = []
        for i in range(num_squeezings):
            squeezings.append(squeeze_group(cur_features))
            cur_features *= 2
        self.squeezings = nn.ModuleList(squeezings)
        self.final_channels = dimension*dimension*3//(2**num_squeezings)
        self.final_dimension = dimension//(2**num_squeezings)
        self.final = FFJORD(LinearBlock(self.final_channels))

    def forward(self, input):
        def squeeze(x):
            b, c, h, w = x.shape
            return x.reshape(b, c*4, h//2, w//2)

        def extract(x, latent_vector):
            b, c, h, w = x.shape
            new_latents = x[:, :c//2].reshape(b, c*h*w//2)
            return (x[:, c//2:], torch.cat([new_latents, latent_vector], 1))
        b, c, h, w = input.shape
        latent = torch.empty((b, 0)).to(input)
        #out = logit((255*input/256+1/512))
        # log_p = torch.log(-261120/((510*input-511) *
        #                           (510*input+1))).sum(3).sum(2).sum(1)
        # log_p = (out - 2*F.softplus(out) +
        #         np.log(256/255)).sum(3).sum(2).sum(1)
        out = input
        log_p = torch.zeros(b).to(input)
        for squeezing in self.squeezings:
            out = squeeze(out)
            (y, y_log_p) = squeezing[0](out)
            log_p += y_log_p
            out = y
            (out, latent) = extract(out, latent)
            for layer in squeezing[1:]:
                (y, y_log_p) = layer(out)
                log_p += y_log_p
                out = y
        out = out.reshape(b, self.final_channels, 1, 1)
        (y, y_log_p) = self.final(out)
        out = y
        latent = torch.cat([out.reshape(b, self.final_channels), latent], 1)
        log_p += y_log_p
        return(latent, log_p)

    def reverse(self, latent):
        def unsqueeze(x):
            b, c, h, w = x.shape
            return x.reshape(b, c//4, h*2, w*2)

        def insert(x, latent):
            b, c, h, w = x.shape
            return (torch.cat([latent[:, :c*h*w].reshape(b, c, h, w), x], 1), latent[:, c*h*w:])
        out = latent[:, :self.final_channels].reshape(
            -1, self.final_channels, 1, 1)
        remaining_latent = latent[:, self.final_channels:]
        (out, log_p) = self.final.reverse(out)
        out = out.reshape(-1, self.final_channels//(self.final_dimension**2),
                          self.final_dimension, self.final_dimension)
        for squeezing in self.squeezings[::-1]:
            for layer in squeezing[1:][::-1]:
                (y, y_log_p) = layer.reverse(out)
                log_p += y_log_p
                out = y
            (out, remaining_latent) = insert(out, remaining_latent)
            (y, y_log_p) = squeezing[0].reverse(out)
            log_p += y_log_p
            out = y
            out = unsqueeze(out)
       # log_p -= (out - 2*F.softplus(out) +
       #           np.log(256/255)).sum(3).sum(2).sum(1)
       # return ((torch.sigmoid(out)-1/512)*256/255, log_p)
        return (out, log_p)


def num_params(model):
    model_parameters = filter(lambda p: p.requires_grad, model.parameters())
    params = sum([np.prod(p.size()) for p in model_parameters])
    return params


def likelyhood_function(x, dims):
    return torch.sum(-x**2, 1)/2 - dims * (np.log(2)+np.log(np.pi))/2

    # out = torch.where(x < 0, 500*x, torch.zeros_like(x))
    # out = torch.where(x > 1, -500*(x-1), out)
    # return torch.sum(out, 1)


if __name__ == "__main__":
    model = MultiscaleFlow(4, 1, 1)
    x = torch.ones(1, 3, 4, 4).expand(1, 3, 4, 4)/10
    out = model(x)
  #  out2 = model(x)
  #  out3 = model(x)
    print((out[1]))
    print(likelyhood_function(out[0], 3*4*4))
    distr = torch.distributions.multivariate_normal.MultivariateNormal(
        loc=torch.zeros_like(out[0]), covariance_matrix=torch.eye(3*4*4))
    print(distr.log_prob(out[0]))
  #  print(out2[1])
  #  print(out3[1])
    #recon = model.reverse(out[0])
    # print(recon[1])
    # print(torch.sum(recon[0]-x))
    # print(torch.sum(torch.sigmoid(logit(x))-x))
    # print(num_params(model))


def train(model, data, batch_size, epochs, device, callback, optimizer, scaler=None):
    print("Beginning training!")

    autograd_enabled = scaler != None

    for epoch in range(epochs):
        print("begin epoch,", epoch)
        for (i, data_batch) in enumerate(data):
            optimizer.zero_grad()

            device_batch = data_batch[0].to(device)

#            dequantization_noise = torch.empty_like(device_batch)
#            dequantization_noise.uniform_(-1/512, 1/512)

#            dequantized = device_batch+dequantization_noise

            [b, c, h, w] = device_batch.shape
            dims = c * h * w
            output = model(device_batch)
            log_likelyhood = likelyhood_function(output[0], dims) + output[1]
            loss = -torch.mean(log_likelyhood)/(dims*np.log(2))
            if loss.isinf() or loss.isnan():
                continue
            loss.backward()
            optimizer.step()
            callback(i, len(data), model, epoch, epochs, loss)
