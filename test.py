import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

# haar_matrix_vert = torch.tensor(
#    [[1, 1], [1, -1]]).reshape((2, 1, 2, 1)).float()/np.sqrt(2)
haar_matrix_vert = torch.tensor(
    [[1, 1], [1, -1]]).reshape((2, 1, 2, 1)).float().expand(2, 1, 2, 1)
haar_matrix_horiz = torch.transpose(haar_matrix_vert, 2, 3)
haar_matrix_horiz_exp = torch.cat(2*[haar_matrix_horiz], 0)
haar_horiz_tranpose = torch.transpose(
    haar_matrix_horiz, 0, 1)

haar = torch.tensor([[[1, 1], [1, 1]], [[1, -1], [1, -1]],
                     [[1, 1], [-1, -1]], [[1, -1], [-1, 1]]])/2.0
haar_t = torch.zeros((12, 3, 2, 2))
haar_t[0:4, 0, :, :] = haar
haar_t[4:8, 1, :, :] = haar
haar_t[8:12, 2, :, :] = haar

# print(haar.shape)

x = torch.tensor(np.linspace(0, 19, 20)).reshape(1, 1, 20).float()


#x = 3*x + x**2 - 1
#:x = torch.sin(3.14*x)
#x.uniform_(0, 1)

# print(x)


test_filter = torch.empty((2, 1, 4)).normal_(0, 1)
test_matrix = test_filter.reshape(2, 4)
test_matrix_inv = torch.transpose(torch.pinverse(test_matrix), 0, 1)
test_filter_inv = test_matrix_inv.reshape((2, 1, 4))

daub_filter = torch.tensor([[[1+np.sqrt(3), 3+np.sqrt(3), 3-np.sqrt(3), 1-np.sqrt(3)]], [
                           [1-np.sqrt(3), -(3-np.sqrt(3)), 3+np.sqrt(3), -(1+np.sqrt(3))]]]).float()/4
daub_matrix = daub_filter.reshape(2, 4)
daub_matrix_inv = torch.transpose(torch.pinverse(daub_matrix), 0, 1)
daub_filter_inv = daub_matrix_inv.reshape((2, 1, 4))
# print(daub_filter_inv)
#print(torch.matmul(torch.transpose(daub_matrix_inv, 0, 1), daub_matrix))
print(daub_filter)


d6_low_pass = [0.035226291882100656,
               -0.08544127388224149,
               -0.13501102001039084,
               0.4598775021193313,
               0.8068915093133388,
               0.3326705529509569,
               ]

d6_high_pass = [-0.3326705529509569,
                0.8068915093133388,
                -0.4598775021193313,
                -0.13501102001039084,
                0.08544127388224149,
                0.035226291882100656,
                ]

d6_filter = torch.tensor([[d6_low_pass], [d6_high_pass]]).float()
d6_matrix = d6_filter.reshape(2, 6)
d6_matrix_inv = torch.transpose(torch.pinverse(d6_matrix), 0, 1)
d6_filter_inv = d6_matrix_inv.reshape((2, 1, 6))

bi22_low_pass = [
    0.0,
    -0.1767766952966369,
    0.3535533905932738,
    1.0606601717798214,
    0.3535533905932738,
    -0.1767766952966369,

]

bi22_high_pass = [
    0.0,
    0.3535533905932738,
    -0.7071067811865476,
    0.3535533905932738,
    0.0,
    0.0,

]

bi_22_low_pass_inv = [
    0.0,
    0.3535533905932738,
    0.7071067811865476,
    0.3535533905932738,
    0.0,
    0.0

]

bi_22_high_pass_inv = [
    0.0,
    0.1767766952966369,
    0.3535533905932738,
    -1.0606601717798214,
    0.3535533905932738,
    0.1767766952966369,

]

bi22_filter = torch.tensor([[bi22_low_pass], [bi22_high_pass]]).float()

bi22_filter_inv = torch.tensor(
    [[bi_22_low_pass_inv], [bi_22_high_pass_inv]]).float()
bi22_filter_inv_m = bi22_filter_inv.reshape(2, 6)
bi22_matrix_inv = torch.transpose(torch.pinverse(bi22_filter_inv_m), 0, 1)
bi22_filter_inv2 = bi22_matrix_inv.reshape((2, 1, 6))

cf1_low_pass = [
    -0.01565572813546454,
    -0.0727326195128539,
    0.38486484686420286,
    0.8525720202122554,
    0.3378976624578092,
    -0.0727326195128539,

]

cf1_high_pass = [
    0.0727326195128539,
    0.3378976624578092,
    -0.8525720202122554,
    0.38486484686420286,
    0.0727326195128539,
    -0.01565572813546454,

]

cf1_filter = torch.tensor([[cf1_low_pass], [cf1_high_pass]]).float()
cf1_matrix = cf1_filter.reshape(2, 6)
cf1_matrix_inv = torch.transpose(torch.pinverse(cf1_matrix), 0, 1)
cf1_filter_inv = cf1_matrix_inv.reshape((2, 1, 6))


#print(torch.matmul(torch.transpose(d6_matrix_inv, 0, 1), d6_matrix))

x_padded = F.pad(x, (2, 2), mode='circular')
x_1 = F.conv1d(x_padded, d6_filter, stride=2)
x_2 = F.conv_transpose1d(x_1, d6_filter_inv, stride=2,
                         padding=0, output_padding=0)
print(x_1.size())
print(x)
print(x_1)
print(x_2)
# print(x_2)
#print(x_1.size(), x_2.size())
#print(x_1[:, :1, :].shape)
x_padded = F.pad(x_1[:, :1, :], (2, 2), mode='circular')
x_2 = F.conv1d(x_padded, d6_filter, stride=2)
# print(x)
# print(x_1)
# print(x_2)
x_padded = F.pad(x_2[:, :1, :], (2, 2), mode='circular')
x_3 = F.conv1d(x_padded, d6_filter, stride=2)
# print(x_3)
