
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as opt
import numpy as np
import torch.cuda.amp as amp
import functools


class ResidualDenseBlock(nn.Module):
    def __init__(self, nf, gc=32, res_scale=0.2, sn=False):
        super().__init__()
        self.sn = sn

        def nonlinearity(in_chans):
            # return EvoNormS02D(
            #     in_chans, np.gcd(in_chans//2, 16))
            return nn.LeakyReLU(negative_slope=0.2)
        if sn:
            spectral_norm = nn.utils.spectral_norm
        else:
            def spectral_norm(x): return x
        self.layer1 = nn.Sequential(
            spectral_norm(nn.Conv2d(nf + 0 * gc, gc, 3, padding=1, bias=True)), nonlinearity(gc))
        self.layer2 = nn.Sequential(
            spectral_norm(nn.Conv2d(nf + 1 * gc, gc, 3, padding=1, bias=True)), nonlinearity(gc))
        self.layer3 = nn.Sequential(
            spectral_norm(nn.Conv2d(nf + 2 * gc, gc, 3, padding=1, bias=True)), nonlinearity(gc))
        self.layer4 = nn.Sequential(
            spectral_norm(nn.Conv2d(nf + 3 * gc, gc, 3, padding=1, bias=True)), nonlinearity(gc))
        self.layer5 = nn.Sequential(
            spectral_norm(nn.Conv2d(nf + 4 * gc, nf, 3, padding=1, bias=True)), nonlinearity(nf))

        self.res_scale = res_scale

    def forward(self, x, styles=None):
        if styles is None:
            layer1 = self.layer1(x)
            layer2 = self.layer2(torch.cat((x, layer1), 1))
            layer3 = self.layer3(torch.cat((x, layer1, layer2), 1))
            layer4 = self.layer4(torch.cat((x, layer1, layer2, layer3), 1))
        else:
            [b, nc] = styles.shape
            stlyes_reshaped = styles.reshape(b, 2, 4, nc//8)
            layer1 = adain(self.layer1(
                x), stlyes_reshaped[:, 0, 0], stlyes_reshaped[:, 1, 0])
            layer2 = adain(self.layer2(torch.cat((x, layer1), 1)),
                           stlyes_reshaped[:, 0, 1], stlyes_reshaped[:, 1, 1])
            layer3 = adain(self.layer3(torch.cat((x, layer1, layer2), 1)),
                           stlyes_reshaped[:, 0, 2], stlyes_reshaped[:, 1, 2])
            layer4 = adain(self.layer4(torch.cat((x, layer1, layer2, layer3), 1)),
                           stlyes_reshaped[:, 0, 3], stlyes_reshaped[:, 1, 3])
        layer5 = self.layer5(torch.cat((x, layer1, layer2, layer3, layer4), 1))
        return layer5.mul(self.res_scale) + x

    def init_weight(self):
        nn.init.kaiming_normal_(self.layer1[0].weight, a=0.2)
        nn.init.normal_(self.layer1[0].bias, std=.02)
        nn.init.kaiming_normal_(self.layer2[0].weight, a=0.2)
        nn.init.normal_(self.layer2[0].bias, std=.02)
        nn.init.kaiming_normal_(self.layer3[0].weight, a=0.2)
        nn.init.normal_(self.layer3[0].bias, std=.02)
        nn.init.kaiming_normal_(self.layer4[0].weight, a=0.2)
        nn.init.normal_(self.layer4[0].bias, std=.02)
       # if not self.sn:
        # nn.init.zeros_(self.layer5[0].weight)
        nn.init.uniform_(self.layer5[0].weight, -.02, 0.02)
        nn.init.zeros_(self.layer5[0].bias)
       # else:
       #     nn.init.kaiming_normal_(self.layer5[0].weight, a=0.2)
       #     nn.init.normal_(self.layer5[0].bias, std=0.02)


def adain(x, new_std, new_mean):
    [b, c, h, w] = x.shape
    old_mean = x.mean(3).mean(2).reshape(b, c, 1, 1)
    zero_mean = x - old_mean
    old_inv_std = torch.rsqrt(
        (zero_mean**2).sum(3).sum(2)/(h*w-1)+.000001).reshape(b, c, 1, 1)
    unit_std = zero_mean*old_inv_std
    return unit_std*new_std.reshape(b, c, 1, 1) + new_mean.reshape(b, c, 1, 1)


class StyleEncoder(nn.Module):
    def __init__(self, nc, layers):
        super().__init__()
        intermediate_channels = nc//32

        intermediate_layers = []
        for i in range(1, layers):
            intermediate_layers.append(
                nn.Sequential(
                    nn.Conv2d(intermediate_channels,
                              intermediate_channels, 3, stride=2, padding=1),
                    nn.LeakyReLU(negative_slope=0.2)))
        self.main = nn.Sequential(*([nn.Sequential(
            nn.Conv2d(12, intermediate_channels, 3, stride=2, padding=1), nn.LeakyReLU())] +
            intermediate_layers)
        )
        self.linear = nn.Linear(nc//2, nc)

    def forward(self, x):
        out = self.main(x)
        out = out.reshape(out.shape[0], out.shape[1]*16)
        return self.linear(out)

    def init_weights(self):
        for parameter in self.main.parameters():
            rank = len(parameter.shape)
            if rank >= 2:
                nn.init.kaiming_normal_(parameter, 0.2)
            else:
                nn.init.normal_(parameter, std=.02)

    def upgrade(self):
        nc = self.main[0][0].weight.data.shape[0]
        device = self.main[0][0].weight.data.device

        new_layer0 = nn.Conv2d(12, nc, 3, stride=2, padding=1)
        new_layer0_weight = torch.rand((nc, 12, 3, 3))*0.001
        for i in range(12):
            new_layer0_weight[i, i, 1, 1] = 1/4
            new_layer0_weight[i, i, 1, 2] = 1/4
            new_layer0_weight[i, i, 2, 1] = 1/4
            new_layer0_weight[i, i, 2, 2] = 1/4
        new_layer0.weight.data = new_layer0_weight.to(device)
        new_layer0.bias.data = torch.zeros(nc).to(device)
        new_layer1 = nn.Conv2d(nc, nc, 3, stride=2, padding=1)
        new_layer1_weight = (torch.rand((nc, nc-12, 3, 3))*0.001).to(device)
        new_layer1.weight.data = torch.cat(
            [self.main[0][0].weight.data, new_layer1_weight], 1)
        new_layer1.bias.data = torch.zeros(nc).to(device)
        new_main = [nn.Sequential(new_layer0, nn.LeakyReLU()), nn.Sequential(
            new_layer1, nn.LeakyReLU())] + list(self.main)[1:]
        self.main = nn.Sequential(*new_main)


class Generator(nn.Module):
    def __init__(self, dimension, n_blocks=8):
        super().__init__()

        self.layers = dimension.bit_length()-3
        self.num_styles = 2*32*4*n_blocks

        def make_layer(block, n_layers):
            layers = []
            for _ in range(n_layers):
                layers.append(block())
            return nn.ModuleList(layers)
        block = functools.partial(ResidualDenseBlock, nf=64, gc=32)

        if self.layers == 0:
            self.style_encoder = nn.Sequential(
                nn.Flatten(),
                nn.Linear(4*4*12, self.num_styles)
            )
        else:
            self.style_encoder = StyleEncoder(self.num_styles, self.layers-1)

        self.conv_first = nn.Conv2d(12, 64, 3, stride=1, padding=1)
        self.main = make_layer(block, n_blocks)
        self.upconv = nn.Conv2d(64, 64, 3, padding=1, stride=1)
        self.HRconv = nn.Conv2d(64, 64, 3, stride=1, padding=1)
        self.conv_last = nn.Conv2d(64, 3, 3, stride=1, padding=1)

    def forward(self, x, noise):
        combined = torch.cat([x, noise], 1)
        styles = self.style_encoder(combined)
        out = self.conv_first(combined)
        for (i, layer) in enumerate(self.main):
            out = layer(out, styles[:, i*2*32*4:(i+1)*2*32*4])
        out = F.leaky_relu_(self.upconv(
            F.interpolate(out, scale_factor=2, mode='nearest')), negative_slope=0.2)
        out = F.leaky_relu_(self.HRconv(out), negative_slope=0.2)
        out = self.conv_last(out)
        return out.float()

    def init_weights(self):
        for mod in [self.style_encoder, self.conv_first, self.upconv, self.HRconv, self.conv_last]:
            for parameter in mod.parameters():
                rank = len(parameter.shape)
                if rank >= 2:
                    nn.init.kaiming_normal_(parameter, 0.2)
                else:
                    nn.init.normal_(parameter, std=.02)
        for layer in self.main:
            layer.init_weight()

    def upgrade(self):
        if self.layers == 0:
            device = self.style_encoder[1].weight.data.device
            new_style_encoder = StyleEncoder(self.num_styles, 0)
            new_linear_weight_0 = self.style_encoder[1].weight.data.reshape(
                12, 4, 4, self.num_styles)
            new_linear_weight_1 = torch.rand(
                self.num_styles//32 - 12, 4, 4, self.num_styles).to(device)*0.001
            new_linear_weight = torch.cat([new_linear_weight_0, new_linear_weight_1], 0).reshape(
                self.num_styles, self.num_styles//2)
            new_style_encoder.linear.weight.data = new_linear_weight
            new_style_encoder.linear.bias.data = self.style_encoder[1].bias.data

            new_layer0 = nn.Conv2d(12, self.num_styles //
                                   32, 3, stride=2, padding=1)
            new_layer0_weight = torch.rand(
                (self.num_styles//32, 12, 3, 3))*0.001
            for i in range(12):
                new_layer0_weight[i, i, 1, 1] = 1/4
                new_layer0_weight[i, i, 1, 2] = 1/4
                new_layer0_weight[i, i, 2, 1] = 1/4
                new_layer0_weight[i, i, 2, 2] = 1/4
            new_layer0.weight.data = new_layer0_weight.to(device)
            new_layer0.bias.data = torch.zeros(self.num_styles//32).to(device)
            new_style_encoder.main[0][0] = new_layer0
            self.style_encoder = new_style_encoder
        else:
            self.style_encoder.upgrade()
        self.layers += 1


# def d_block(in_channel, out_channel):
    #    return nn.Sequential(
    #        nn.utils.spectral_norm(
    #            nn.Conv2d(in_channel, out_channel, 3, stride=1, padding=1)),
    #        nn.LeakyReLU(0.2, inplace=True),
    #        nn.utils.spectral_norm(
    #            nn.Conv2d(out_channel, out_channel, 3, stride=1, padding=1)),
    #        nn.LeakyReLU(0.2, inplace=True),
    #        nn.AvgPool2d(2)
    #    )
def d_block(nf):
    return nn.Sequential(ResidualDenseBlock(nf, sn=True), nn.AvgPool2d(2))


class Discriminator(nn.Module):
    def __init__(self, dimension, nc=64):
        super().__init__()
        self.layers = dimension.bit_length()-2
        self.nc = nc
        self.intro_block = nn.Conv2d(3, nc, 3, stride=1, padding=1)
        layers_main = []
        for i in range(10):
            layers_main.append(ResidualDenseBlock(nc, sn=True))
        self.main = nn.Sequential(*layers_main)
        layers = []
        for i in range(0, self.layers):
            layers.append(d_block(nc))
        self.linear = nn.Sequential(
            nn.Flatten(),
            nn.utils.spectral_norm(nn.Linear(nc*16, 1))
        )
        self.downscale = nn.Sequential(*layers)

    def forward(self, x):
        out = self.intro_block(x)
        out = self.main(out)
        out = self.downscale(out)
        out = self.linear(out)
        return out.float()

    def init_weights(self):
        #    for parameter in self.parameters():
        #        rank = len(parameter.shape)
        #        if rank >= 2:
        #            nn.init.kaiming_normal_(parameter, 0.2)
        #        else:
        #            nn.init.normal_(parameter, std=.02)
        for layer in [self.intro_block, self.linear]:
            for parameter in layer.parameters():
                rank = len(parameter.shape)
                if rank >= 2:
                    nn.init.kaiming_normal_(parameter, 0.2)
                else:
                    nn.init.normal_(parameter, std=.02)
        for layer in self.main:
            layer.init_weight()
        for layer in self.downscale:
            layer[0].init_weight()

    def upgrade(self):
        self.layers += 1
       # new_block0 = d_block(3, self.nc)
       # new_block1 = d_block(self.nc, self.nc)
       # for layer in [new_block0, new_block1]:
       #     for parameter in layer.parameters():
       #         rank = len(parameter.shape)
       #         if rank >= 2:
       #             nn.init.kaiming_normal_(parameter, 0.2)
       #         else:
       #             nn.init.normal_(parameter, std=0.2)
       # new_layers = [new_block0, new_block1] + list(self.main[1:])
       # self.main = nn.Sequential(*new_layers)
        new_layer = d_block(self.nc)
        new_layer[0].init_weight()
        new_layer.to(self.intro_block.weight.device)
        self.downscale = nn.Sequential(*(list(self.downscale)+[new_layer]))


def num_params(model):
    model_parameters = filter(lambda p: p.requires_grad, model.parameters())
    params = sum([np.prod(p.size()) for p in model_parameters])
    return params


if __name__ == "__main__":
    gen = Generator(4, n_blocks=8)
    disc = Discriminator(4)
    gen.init_weights()
    disc.init_weights()
    # disc.upgrade()
    # gen.upgrade()
    # gen.upgrade()
    # print(gen)
    print(num_params(gen))
    print(num_params(disc))


def train(gen, disc, data, batch_size, epochs, device, callback, optimizer_g, optimizer_d, prev_gen=None, scaler=None, pl_mul=1):
    print("Beginning training!")

    autograd_enabled = scaler != None

    assert batch_size % 3 == 0

    pixel_loss = 0
    gan_loss_gen = 0
    gan_loss_disc = 0

    for epoch in range(epochs):
        print("begin epoch,", epoch)
        for (i, data_batch) in enumerate(data):

            generator_step = i % 2 == 1
            optimizer_d.zero_grad()
            optimizer_g.zero_grad()

            device_batch = data_batch[0].to(device)
            if len(device_batch) != batch_size:
                continue
            batch_reals = device_batch[:2*batch_size//3]
            batch_fakes = device_batch[2*batch_size//3:]
            batch_fakes_downscaled = F.avg_pool2d(batch_fakes, 2)
            if prev_gen is None:
                gen_fakes = torch.empty_like(batch_fakes_downscaled)
                gen_fakes.normal_()
                gen_fakes_2 = torch.empty_like(gen_fakes)
                gen_fakes_2.normal_()
            else:
                gen_fakes = prev_gen(batch_size//3)
            all_fakes = torch.cat([batch_fakes_downscaled, gen_fakes], 0)

            [b, c, h, w] = all_fakes.shape

            noise = torch.empty(b, c*3, h, w).to(device)
            noise.normal_()
            if generator_step:
                upscaled = gen(all_fakes, noise)
            else:
                with torch.no_grad():
                    upscaled = gen(all_fakes, noise)

            disc_reals = disc(batch_reals)
            disc_fakes = disc(upscaled)

            if generator_step:
                pixel_loss = torch.mean(
                    torch.abs(upscaled[:batch_size//3]-batch_fakes))
                gan_loss_gen = ragan_loss_gen(disc_reals, disc_fakes)
                flood_loss_gen = torch.where(
                    gan_loss_gen < 1, -gan_loss_gen, gan_loss_gen)

                loss = pl_mul*pixel_loss + flood_loss_gen
                # loss = pixel_loss
                if autograd_enabled:
                    scaler.scale(loss).backward()
                    scaler.step(optimizer_g)
                else:
                    loss.backward()
                    optimizer_g.step()
            else:
                gan_loss_disc = ragan_loss_disc(disc_reals, disc_fakes)
                flood_loss_disc = torch.where(
                    gan_loss_disc < 1, -gan_loss_disc, gan_loss_disc)

                if autograd_enabled:
                    scaler.scale(flood_loss_disc).backward()
                    scaler.step(optimizer_d)
                else:
                    flood_loss_disc.backward()
                    optimizer_d.step()
            # scaler.update()

            callback(i, len(data), gen, epoch, epochs,
                     pixel_loss, gan_loss_gen, gan_loss_disc)


def hinge_loss_disc(reals, fakes):
    return F.relu(1-torch.mean(reals)) + F.relu(1+torch.mean(fakes))


def hinge_loss_gen(reals, fakes):
    return -torch.mean(fakes)


def ragan_loss_disc(reals, fakes):
    d_reals_fakes = (reals-torch.mean(fakes))
    d_fakes_reals = (fakes-torch.mean(reals))
    return F.binary_cross_entropy_with_logits(d_reals_fakes, torch.ones_like(d_reals_fakes)) + F.binary_cross_entropy_with_logits(d_fakes_reals, torch.zeros_like(d_fakes_reals))


def ragan_loss_gen(reals, fakes):
    d_reals_fakes = (reals-torch.mean(fakes))
    d_fakes_reals = (fakes-torch.mean(reals))
    return F.binary_cross_entropy_with_logits(d_reals_fakes, torch.zeros_like(d_reals_fakes)) + F.binary_cross_entropy_with_logits(d_fakes_reals, torch.ones_like(d_fakes_reals))
