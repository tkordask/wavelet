import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as opt
import torchdiffeq as tde
import numpy as np


class GProb(nn.Module):
    def __init__(self, nc):
        super().__init__()
        self.main = nn.Sequential(
            nn.Softplus(),
            nn.Conv2d(nc, nc, 3, stride=1, padding=1),
            nn.Softplus(),

            nn.Conv2d(nc, nc, 3, stride=1, padding=1),
            nn.Softplus(),

            nn.Conv2d(nc, nc, 3, stride=1, padding=1),
            nn.Softplus(),

            nn.Conv2d(nc, nc, 3, stride=1, padding=1),

        )
        for parameter in self.main.parameters():
            rank = len(parameter.shape)
            if rank >= 2:
                nn.init.kaiming_normal_(parameter, 0.2)
            else:
                nn.init.normal_(parameter, std=.02)
        nn.init.zeros_(self.main[-1].weight)
        nn.init.zeros_(self.main[-1].bias)

    def forward(self, t, x):
        return self.main(x)


class GProbLin(nn.Module):
    def __init__(self, nc):
        super().__init__()
        self.main = nn.Sequential(
            nn.Softplus(),
            nn.Linear(nc, nc),
            nn.Softplus(),
            nn.Linear(nc, nc)
        )
        for parameter in self.main.parameters():
            rank = len(parameter.shape)
            if rank >= 2:
                nn.init.kaiming_normal_(parameter, 0.2)
            else:
                nn.init.normal_(parameter, std=.02)
        nn.init.zeros_(self.main[-1].weight)
        nn.init.zeros_(self.main[-1].bias)

    def forward(self, t, x):
        return self.main(x)


class GBlock(nn.Module):
    def __init__(self, nc, upsample=True, downsample=False):
        super().__init__()
        self.problem = GProb(nc)
        if upsample:
            self.upsample_conv = nn.ConvTranspose2d(
                nc, nc, 2, stride=2, output_padding=0)
            nn.init.kaiming_normal_(self.upsample_conv.weight)
            nn.init.normal_(self.upsample_conv.bias, std=0.02)
        self.upsample = upsample
        self.downsample = downsample

    def forward(self, x):
        # out = tde.odeint_adjoint(
        #    self.problem, x, torch.linspace(0, 1, 2).to(x.device))[1]
        out = x+self.problem(0, x)
        if self.upsample:
            out = self.upsample_conv(out)
        if self.downsample:
            out = F.avg_pool2d(out, 2)
        return out


class Generator(nn.Module):
    def __init__(self, nc=8):
        super().__init__()
        self.nc = nc
        self.main = nn.Sequential(
            GBlock(nc),  # 4x4 -> 8x8
            GBlock(nc),  # 8x8 -> 16x16
            GBlock(nc),  # 16x16 -> 32x32
            GBlock(nc),  # 32x32 -> 64x64
            GBlock(nc, upsample=False)
        )
        self.linear = GProbLin(128)
        self.linear_2 = nn.Linear(128, 4*4*nc)
        self.out_conv = nn.Conv2d(nc, 3, 1)
        nn.init.kaiming_normal_(self.linear_2.weight)
        nn.init.normal_(self.linear_2.bias, 0.02)
        nn.init.kaiming_normal_(self.out_conv.weight)
        nn.init.normal_(self.out_conv.bias, 0.02)

    def forward(self, x):
        out = tde.odeint_adjoint(
            self.linear, x, torch.linspace(0, 1, 2).to(x.device))[1]
        out = self.linear_2(out)
        out = out.reshape(-1, self.nc, 4, 4)
        out = self.main(out)
        return torch.tanh(self.out_conv(out))


class Discriminator(nn.Module):
    def __init__(self, nc=8):
        super().__init__()
        self.nc = nc
        self.in_conv = nn.Conv2d(3, nc, 1)
        self.main = nn.Sequential(
            GBlock(nc, upsample=False, downsample=True),  # 64x64 -> 32x32
            GBlock(nc, upsample=False, downsample=True),  # 32x32 -> 16x16
            GBlock(nc, upsample=False, downsample=True),  # 16x16 -> 8x8
            GBlock(nc, upsample=False, downsample=True),  # 8x8 -> 4x4
        )
        self.linearProb = GProbLin(4*4*nc)
        self.linear = nn.Linear(4*4*nc, 1)

    def forward(self, x):
        out = self.in_conv(x)
        out = self.main(out)
        out = out.reshape(-1, 4*4*self.nc)
        # out = tde.odeint_adjoint(self.linearProb, out,
        #                         torch.linspace(0, 1, 2).to(x.device))[1]
        out = self.linear(out)
        return out


def num_params(model):
    model_parameters = filter(lambda p: p.requires_grad, model.parameters())
    params = sum([np.prod(p.size()) for p in model_parameters])
    return params


if __name__ == "__main__":
    gen = Generator()
    disc = Discriminator()
    print(num_params(gen))
    print(num_params(disc))
    noise = torch.rand(1, 128)
    img = gen(noise)
    discirmination = disc(img)
    print(img.shape)
    print(discirmination.shape)


def train(gen, disc, data, batch_size, epochs, device, callback, optimizer_g, optimizer_d, prev_gen=None, scaler=None, pl_mul=1):
    print("Beginning training!")

    autograd_enabled = scaler != None

    gan_loss_gen = 0
    gan_loss_disc = 0

    for epoch in range(epochs):
        print("begin epoch,", epoch)
        for (i, data_batch) in enumerate(data):

            generator_step = i % 2 == 1
            optimizer_d.zero_grad()
            optimizer_g.zero_grad()

            device_batch = data_batch[0].to(device)
            if len(device_batch) != batch_size:
                continue
            batch_reals = device_batch
            if prev_gen is None:
                gen_fakes = torch.empty(
                    (device_batch.shape[0], 128)).to(device)
                gen_fakes.normal_()

            if generator_step:
                batch_fakes = gen(gen_fakes)
            else:
                with torch.no_grad():
                    batch_fakes = gen(gen_fakes)

            disc_reals = disc(batch_reals)
            disc_fakes = disc(batch_fakes)

            if generator_step:
                gan_loss_gen = ragan_loss_gen(disc_reals, disc_fakes)
                flood_loss_gen = torch.where(
                    gan_loss_gen < 1, -gan_loss_gen, gan_loss_gen)

                loss = flood_loss_gen
                # loss = pixel_loss
                if autograd_enabled:
                    scaler.scale(loss).backward()
                    scaler.step(optimizer_g)
                else:
                    loss.backward()
                    optimizer_g.step()
            else:
                gan_loss_disc = ragan_loss_disc(disc_reals, disc_fakes)
                flood_loss_disc = torch.where(
                    gan_loss_disc < 1, -gan_loss_disc, gan_loss_disc)

                if autograd_enabled:
                    scaler.scale(gan_loss_disc).backward()
                    scaler.step(optimizer_d)
                else:
                    flood_loss_disc.backward()
                    optimizer_d.step()
            # scaler.update()

            callback(i, len(data), gen, epoch, epochs,
                     gan_loss_gen, gan_loss_disc)


def hinge_loss_disc(reals, fakes):
    return F.relu(1-torch.mean(reals)) + F.relu(1+torch.mean(fakes))


def hinge_loss_gen(reals, fakes):
    return -torch.mean(fakes)


def ragan_loss_disc(reals, fakes):
    d_reals_fakes = (reals-torch.mean(fakes))
    d_fakes_reals = (fakes-torch.mean(reals))
    return F.binary_cross_entropy_with_logits(d_reals_fakes, torch.ones_like(d_reals_fakes)) + F.binary_cross_entropy_with_logits(d_fakes_reals, torch.zeros_like(d_fakes_reals))


def ragan_loss_gen(reals, fakes):
    d_reals_fakes = (reals-torch.mean(fakes))
    d_fakes_reals = (fakes-torch.mean(reals))
    return F.binary_cross_entropy_with_logits(d_reals_fakes, torch.zeros_like(d_reals_fakes)) + F.binary_cross_entropy_with_logits(d_fakes_reals, torch.ones_like(d_fakes_reals))
